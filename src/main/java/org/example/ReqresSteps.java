package org.example;

import org.example.models.*;
import org.junit.jupiter.api.Assertions;

import static io.restassured.RestAssured.given;

public class ReqresSteps {

    private static final String SINGLE_USER_ENDPOINT = "/users/%s";     // Необходимо указать id пользователя
    private static final String POST_REGISTER_ENDPOINT = "/register";
    private static final String PUT_UPDATE_USER_ENDPOINT = "/users/%s"; // Необходимо указать id пользователя
    private static final String DELETE_USER_ENDPOINT = "/users/%s";     // Необходимо указать id пользователя

    public SingleUserData getSingleUserDataById(String id, int expectedCode) {
        return given()
                .when()
                .spec(ReqresSpecs.getBaseRequestSpec())
                .get(SINGLE_USER_ENDPOINT.formatted(id))
                .then()
                .spec(ReqresSpecs.getBaseResponseSpec(expectedCode))
                .extract().body().jsonPath().getObject("data.", SingleUserData.class);
    }

    public void checkUserExist(SingleUserData userData) {
        Assertions.assertNotNull(userData, "Пользователь не существует");
    }

    public void checkUserNotExist(SingleUserData userData) {
        Assertions.assertNull(userData, "Пользователь существует");
    }

    public RegistrationResponseData registerUser(RegistrationData registrationData, int expectedCode) {
        return given()
                .when()
                .spec(ReqresSpecs.getBaseRequestSpec())
                .body(registrationData)
                .post(POST_REGISTER_ENDPOINT)
                .then()
                .spec(ReqresSpecs.getBaseResponseSpec(expectedCode))
                .extract().body().jsonPath().getObject(".", RegistrationResponseData.class);
    }

    public void checkRegistrationSuccess(RegistrationResponseData data, String email) {
        Assertions.assertNull(data.getError(), "Регистрация не удалась");
        SingleUserData userData = getSingleUserDataById(data.getId().toString(), 200);
        Assertions.assertEquals(email, userData.getEmail());
    }

    public void checkRegistrationFail(RegistrationResponseData data) {
        Assertions.assertNotNull(data.getError(), "Регистрация удалась");
    }

    public UpdateUserResponseData updateUserById(
            String userId,
            UpdateUserData data,
            int expectedCode
    ) {
        return given()
                .when()
                .spec(ReqresSpecs.getBaseRequestSpec())
                .body(data)
                .put(PUT_UPDATE_USER_ENDPOINT.formatted(userId))
                .then()
                .spec(ReqresSpecs.getBaseResponseSpec(expectedCode))
                .extract().body().jsonPath().getObject(".", UpdateUserResponseData.class);
    }

    public void checkUserUpdate(String id, UpdateUserResponseData data) {
        SingleUserData userData = getSingleUserDataById(id, 200);
        Assertions.assertEquals(data.getEmail(), userData.getEmail(), "Электронная почта не была обновлена");
        Assertions.assertEquals(data.getFirstName(), userData.getFirstName(), "Имя не было обновлено");
    }

    public void deleteUserById(String id, int expectedCode) {
        given()
                .when()
                .spec(ReqresSpecs.getBaseRequestSpec())
                .delete(DELETE_USER_ENDPOINT.formatted(id))
                .then()
                .spec(ReqresSpecs.getBaseResponseSpec(expectedCode));
    }

    public void checkUserDelete(String id) {
        SingleUserData data = getSingleUserDataById(id, 404);
        Assertions.assertNull(data, "Данные пользователя не были удалены");
    }
}
