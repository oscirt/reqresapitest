package org.example.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RegistrationResponseData {
    private Integer id;
    private String token;
    private String error;
}
