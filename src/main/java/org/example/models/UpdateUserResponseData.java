package org.example.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UpdateUserResponseData {
    @JsonProperty("first_name")
    private String firstName;
    private String email;
    private String updatedAt;
}
