import org.example.*;
import org.example.models.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;

public class ReqresTest {

    private final static ReqresSteps steps  = new ReqresSteps();

    @ParameterizedTest
    @ValueSource(strings = {"1", "2", "3"})
    public void positiveGetUserTest(String id) {
        SingleUserData userData = steps.getSingleUserDataById(id, 200);
        steps.checkUserExist(userData);
    }

    @ParameterizedTest
    @ValueSource(strings = {"-1", "13", "100"})
    public void negativeGetUserTest(String id) {
        SingleUserData userData = steps.getSingleUserDataById(id, 404);
        steps.checkUserNotExist(userData);
    }

    @ParameterizedTest
    @CsvFileSource(resources = {"/test_data/positive_user_registration_data.csv"}, numLinesToSkip = 1)
    public void positiveUserRegistration(String email, String password) {
        RegistrationData registrationData = new RegistrationData(email, password);
        RegistrationResponseData responseData = steps.registerUser(registrationData, 200);
        steps.checkRegistrationSuccess(responseData, email);
    }

    @ParameterizedTest
    @CsvFileSource(resources = {"/test_data/negative_user_registration_data.csv"}, numLinesToSkip = 1)
    public void negativeUserRegistration(String email, String password) {
        RegistrationData registrationData = new RegistrationData(email, password);
        RegistrationResponseData responseData = steps.registerUser(registrationData, 400);
        steps.checkRegistrationFail(responseData);
    }

    @ParameterizedTest
    @CsvFileSource(resources = {"/test_data/positive_user_update_data.csv"}, numLinesToSkip = 1)
    public void positiveUserUpdateData(String id, String name, String job) {
        UpdateUserData data = new UpdateUserData(name, job);
        UpdateUserResponseData responseData = steps.updateUserById(id, data, 200);
        steps.checkUserUpdate(id, responseData);
    }

    @ParameterizedTest
    @CsvFileSource(resources = {"/test_data/negative_user_update_data.csv"}, numLinesToSkip = 1)
    public void negativeUserUpdateData(String id, String firstName, String email) {
        UpdateUserData data = new UpdateUserData(firstName, email);
        UpdateUserResponseData responseData = steps.updateUserById(id, data, 200);
        steps.checkUserUpdate(id, responseData);
    }

    @ParameterizedTest
    @ValueSource(strings = {"1", "2", "3"})
    public void positiveUserDelete(String id) {
        steps.deleteUserById(id, 204);
        steps.checkUserDelete(id);
    }

    @ParameterizedTest
    @ValueSource(strings = {"100", "200", "300"})
    public void negativeUserDelete(String id) {
        steps.deleteUserById(id, 204);
        steps.checkUserDelete(id);
    }
}
